﻿namespace PiApproximization
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.termsPromptLabel = new System.Windows.Forms.Label();
            this.termsTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.presentOutputLabel = new System.Windows.Forms.Label();
            this.piOutputLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // termsPromptLabel
            // 
            this.termsPromptLabel.Location = new System.Drawing.Point(12, 53);
            this.termsPromptLabel.Name = "termsPromptLabel";
            this.termsPromptLabel.Size = new System.Drawing.Size(125, 23);
            this.termsPromptLabel.TabIndex = 0;
            this.termsPromptLabel.Text = "Enter a number of terms:";
            this.termsPromptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // termsTextBox
            // 
            this.termsTextBox.Location = new System.Drawing.Point(192, 55);
            this.termsTextBox.Name = "termsTextBox";
            this.termsTextBox.Size = new System.Drawing.Size(100, 20);
            this.termsTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(15, 117);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(122, 50);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "CALCULATE";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // presentOutputLabel
            // 
            this.presentOutputLabel.Location = new System.Drawing.Point(15, 212);
            this.presentOutputLabel.Name = "presentOutputLabel";
            this.presentOutputLabel.Size = new System.Drawing.Size(280, 23);
            this.presentOutputLabel.TabIndex = 3;
            // 
            // piOutputLabel
            // 
            this.piOutputLabel.Location = new System.Drawing.Point(15, 244);
            this.piOutputLabel.Name = "piOutputLabel";
            this.piOutputLabel.Size = new System.Drawing.Size(280, 23);
            this.piOutputLabel.TabIndex = 4;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(107, 293);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(106, 35);
            this.clearButton.TabIndex = 5;
            this.clearButton.Text = "CLEAR";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 347);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.piOutputLabel);
            this.Controls.Add(this.presentOutputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.termsTextBox);
            this.Controls.Add(this.termsPromptLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Approximate Pi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label termsPromptLabel;
        private System.Windows.Forms.TextBox termsTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label presentOutputLabel;
        private System.Windows.Forms.Label piOutputLabel;
        private System.Windows.Forms.Button clearButton;
    }
}

