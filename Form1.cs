﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PiApproximization
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double terms;
            double piApprox = 0;

            //Converts textbox input into a double.
            if (double.TryParse(termsTextBox.Text, out terms))
            {
                //Uses Leibniz's formula for calculating Pi * 4.
                int sign = 1;
                for (double i = 0; i < terms; i++) {                  
                    piApprox += sign * (4 / ((2 * i) + 1));
                    sign *= -1;
                }
              
                //The number of terms being used is converted into a string.
                //presentOutputLabel then indicates that those terms are being used to calculate Pi.
                presentOutputLabel.Text = "Approximate value of Pi after " + terms.ToString() + " terms = ";

                //The approximate value of Pi based on the number of terms given is converted into a string.
                //This string is then written into piOutputLabel.
                piOutputLabel.Text = piApprox.ToString();

              //If textbox input was invalid, a message box prompts the user to enter a number.
            } else
            {
                MessageBox.Show("Please enter a number!");
            }

        }

       //Clears all textbox input and label output.
        private void clearButton_Click(object sender, EventArgs e)
        {
            termsTextBox.Text = "";
            presentOutputLabel.Text = "";
            piOutputLabel.Text = "";
        }
    }
}
